import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    mr = df[df["Name"].str.contains("Mr\.")]
    mrs = df[df["Name"].str.contains("Mrs\.")]
    miss = df[df["Name"].str.contains("Miss\.")]

    mr_num_of_missing_age = mr["Age"].isna().sum()
    mrs_num_of_missing_age = mrs["Age"].isna().sum()
    miss_num_of_missing_age = miss["Age"].isna().sum()

    mr_age_median = round(mr["Age"].dropna().median())
    mrs_age_median = round(mrs["Age"].median())
    miss_age_median = round(miss["Age"].median())

    return [
        ('Mr.', mr_num_of_missing_age, mr_age_median), 
        ('Mrs.', mrs_num_of_missing_age, mrs_age_median), 
        ('Miss.', miss_num_of_missing_age, miss_age_median)
    ]